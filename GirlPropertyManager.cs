﻿using UnityEngine;
using System.Collections;

public class GirlPropertyManager : MonoBehaviour {
    [SerializeField]
    string caption;

    public string Caption { get { return caption; } }

    [SerializeField]
    int currentOutfit = 0;

    public int CurrentOutfit { get { return currentOutfit;  } }

    [SerializeField]
    int currentPose = 0;

    public int CurrentPose { get { return currentPose; } }

    public Emotion ActiveEmotion
    {
        get
        {
            var activePose = poses[CurrentPose];
            return activePose.GetComponent<ChangeEmotion>().ActiveEmotion;
        }
    }

    public Outfit ActiveOutfit
    {
        get
        {
            var activePose = poses[CurrentPose];
            return activePose.GetComponent<ChangeOutfit>().ActiveOutfit;
        }
    }

    [SerializeField]
    bool syncOutfits = true;

    public GameObject[] poses;

    ChangeOutfit[] posesOutfits;

    void Start()
    {
        StoreOutfits();
    }

    public void SetPose( int index )
    {
        if( currentPose == index )
            return;

        currentPose = index;
        for (int i = 0; i < poses.Length; i++)
        {
            if (i == index)
            {
                poses[i].SetActive(true);
            }
            else
            {
                poses[i].SetActive(false);
            }
        }
    }

    private void StoreOutfits()
    {
        posesOutfits = new ChangeOutfit[poses.Length];

        for (int i = 0; i < poses.Length; i++)
        {
            posesOutfits[i] = poses[i].GetComponent<ChangeOutfit>();
        }
    }

    public void SetCurrentOutfit()
    {
        if( currentOutfit == -1)
        {
            DeactivateOutfit();
            return;
        }

        if( syncOutfits )
            for (int i = 0; i < posesOutfits.Length; i++)
            {
                posesOutfits[i].SetActive(currentOutfit);
            }
        else
            posesOutfits[currentPose].SetActive(currentOutfit);
    }

    public void DeactivateOutfit()
    {
        currentOutfit = -1;

        if (syncOutfits)
            for (int i = 0; i < posesOutfits.Length; i++)
            {
                posesOutfits[i].Deactivate();
            }
        else
            posesOutfits[currentPose].Deactivate();
    }

    public void SetOutfit( int outfitId )
    {
        if (currentOutfit == outfitId)
            return;

        currentOutfit = outfitId;

        SetCurrentOutfit();
    }
}
