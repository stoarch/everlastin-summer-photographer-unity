﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class TweenManager : MonoBehaviour {

    public void RestartTween( string id )
    {
        var tween = HOTween.GetTweensById(id, false);
        if( tween.Count == 0 )
        {
            Debug.LogWarningFormat("Tween by id:{0} not found", id);
            return;
        }
        tween[0].Restart();
    }

    public void RewindTween( string id )
    {
        var tween = HOTween.GetTweensById(id, false);
        if( tween.Count == 0 )
        {
            Debug.LogWarningFormat("Tween by id:{0} not found", id);
            return;
        }
        tween[0].Rewind();
    }

    public void StartTween(int id)
    {
        var tween = HOTween.GetTweensByIntId(id, false);
        if( tween.Count == 0 )
        {
            Debug.LogWarningFormat("Tween by id:{0} not found", id);
            return;
        }
        tween[0].Play();
    }

    public void PlayBackwardsTween( string id )
    {
        var tween = HOTween.GetTweensById(id, false);
        if( tween.Count == 0 )
        {
            Debug.LogWarningFormat("Tween by id:{0} not found", id);
            return;
        }
        tween[0].PlayBackwards();
    }

    public void PlayForwardTween( string id )
    {
        var tween = HOTween.GetTweensById(id, false);
        if( tween.Count == 0 )
        {
            Debug.LogWarningFormat("Tween by id:{0} not found", id);
            return;
        }
        tween[0].PlayForward();
    }

    public void StartTween( string id )
    {
        var tween = HOTween.GetTweensById(id, false);
        if( tween.Count == 0 )
        {
            Debug.LogWarningFormat("Tween by id:{0} not found", id);
            return;
        }
        tween[0].Play();
    }
}
