﻿using UnityEngine;
using System.Collections;

public class MoveStraightTillHit : MonoBehaviour {

    public GameObject hitEnd;
    public float horizontalSpeed = 1.0F;
    public float waitNextTime = 3.0F;

    private Vector3 startPos;
    private bool isWaiting = false;

	void Start () {
        startPos = transform.position;	
	}
	
	void Update () 
    {
        if( !isWaiting )
            transform.position = new Vector3(transform.position.x + horizontalSpeed, transform.position.y, transform.position.z);
	}

    void OnTriggerEnter2D( Collider2D collider )
    {
        if (collider.gameObject == hitEnd)
        {
            isWaiting = true;
            transform.position = startPos;
            StartCoroutine(WaitTillMoving());

        }
    }

    private IEnumerator WaitTillMoving()
    {
        yield return new WaitForSeconds(waitNextTime);
        isWaiting = false;
    }

}
