﻿using UnityEngine;
using System.Collections;

public class GirlManager : MonoBehaviour {
    public GirlPropertyManager[] girls;
    public int activeGirl;

    public void SetActiveGirl( int girl )
    {
        activeGirl = girl;
    }

    public GirlPropertyManager ActiveGirlManager
    {
        get
        {
            return girls[activeGirl];

        }
    }


    internal string ActiveGirlDescription()
    {
        return ActiveGirlManager.Caption + "-pose-" + ActiveGirlManager.CurrentPose.ToString() + '-' + ActiveGirlManager.ActiveOutfit.Caption + '-' + ActiveGirlManager.ActiveEmotion.Caption;
    }
}
