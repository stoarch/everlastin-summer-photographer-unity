﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Emotion
{
    public string Caption;
    public SpriteRenderer Display;
}

public class ChangeEmotion : MonoBehaviour {

    public Emotion[] emotions;

    public int activeEmotion = 0;

    public Emotion ActiveEmotion
    {
        get
        {
            return emotions[activeEmotion];
        }
    }

    public void SetActive(int index)
    {
        if( index < 0 || index > emotions.Length)
        {
            Debug.LogError(String.Format("Emotion index is invalid:{0} ({1}..{2})",index, 0, emotions.Length ));
            return;
        }
        
        emotions[activeEmotion].Display.enabled = false;
        activeEmotion = index;
        emotions[activeEmotion].Display.enabled = true;

    }
}
