﻿using UnityEngine;
using System.Collections;

public class GirlOutfitUIManager : MonoBehaviour {

    public PoseOutfitUIManager[] posesUI;
    public GirlPropertyManager girlProp;

    public void Select( int index )
    {
        for (int i = 0; i < posesUI.Length; i++)
        {
            posesUI[i].Select(index);
        }
    }

    public void SelectFromGirlProp()
    {
        Select(girlProp.CurrentOutfit);
    }
}
