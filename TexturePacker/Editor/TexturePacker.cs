﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class TexturePacker : ScriptableWizard {

    public Texture2D[] Textures;

    [SerializeField]
    string AtlasName = "Atlas_Texture";

    [SerializeField]
    int Padding = 4;

    [MenuItem("GameObject/Create Other/Atlas Texture")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Create Atlas", typeof(TexturePacker));
    }

    void OnEnable()
    {
        List<Texture2D> TextureList = new List<Texture2D>();
        if( Selection.objects != null && Selection.objects.Length > 0 )
        {
            Object[] objects = EditorUtility.CollectDependencies(Selection.objects);

            foreach(Object o in objects)
            {
                Texture2D tex = o as Texture2D;

                if( tex != null )
                {
                    TextureList.Add(tex);
                }
            }
        }

        if(TextureList.Count > 0)
        {
            Textures = new Texture2D[TextureList.Count];

            for (int i = 0; i < TextureList.Count; i++)
            {
                Textures[i] = TextureList[i];
            }
        }
    }

    public void ConfigureForAtlas(string TexturePath)
    {
        TextureImporter TexImport = AssetImporter.GetAtPath(TexturePath) as TextureImporter;
        TextureImporterSettings tiSettings = new TextureImporterSettings();
        TexImport.textureType = TextureImporterType.Default;
        TexImport.ReadTextureSettings(tiSettings);

        tiSettings.mipmapEnabled = false;
        tiSettings.readable = true;
        tiSettings.maxTextureSize = 4096;
        tiSettings.textureFormat = TextureImporterFormat.ARGB32;
        tiSettings.filterMode = FilterMode.Point;
        tiSettings.wrapMode = TextureWrapMode.Clamp;
        tiSettings.npotScale = TextureImporterNPOTScale.None;
        
        TexImport.SetTextureSettings(tiSettings);
        AssetDatabase.ImportAsset(TexturePath, ImportAssetOptions.ForceUpdate);
        AssetDatabase.Refresh();
    }

    public void GenerateAtlas()
    {
        GameObject AtlasObject = new GameObject("obj_" + AtlasName);
        AtlasData AtlasComp = AtlasObject.AddComponent<AtlasData>();
        AtlasComp.TextureNames = new string[Textures.Length];
        for (int i = 0; i < Textures.Length; i++)
        {
            string TexturePath = AssetDatabase.GetAssetPath(Textures[i]);
            ConfigureForAtlas(TexturePath);
            AtlasComp.TextureNames[i] = TexturePath;
        }

        Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false);
        AtlasComp.UVs = tex.PackTextures(Textures, Padding, 4096);

        string AssetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/" + AtlasName + ".png");
        byte[] bytes = tex.EncodeToPNG();

        System.IO.File.WriteAllBytes(AssetPath, bytes);
        bytes = null;

        UnityEngine.Object.DestroyImmediate(tex);
        AssetDatabase.ImportAsset(AssetPath);
        AtlasComp.AtlasTexture = AssetDatabase.LoadAssetAtPath(AssetPath, typeof(Texture2D)) as Texture2D;
        ConfigureForAtlas(AssetDatabase.GetAssetPath(AtlasComp.AtlasTexture));

        AssetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/atlasdata_" + AtlasName + ".prefab");
        Object prefab = PrefabUtility.CreateEmptyPrefab(AssetPath);
        PrefabUtility.ReplacePrefab(AtlasObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        DestroyImmediate(AtlasObject);
    }

    void OnWizardCreate()
    {
        GenerateAtlas();
    }
}
