﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class BackgroundManager : MonoBehaviour {

    public List<string> backFileList;

	void Awake() {
        LoadBackgroundList();
	
	}

    private void LoadBackgroundList()
    {
        backFileList = new List<string>();
        string myPath = "Assets/Art/Backgrounds/Resources/";

        DirectoryInfo dir = new DirectoryInfo(myPath);
        FileInfo[] info = dir.GetFiles("*.jpg");
        foreach (FileInfo f in info)
        {
            backFileList.Add(f.Name);
        }
    }
	
}
