﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class BatchRename : ScriptableWizard {
    public string BaseName = "MyObject_";

    public int StartNumber = 0;
    public int Increment = 1;

    [MenuItem("Edit/Batch rename...")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Batch Rename", typeof(BatchRename), "Rename");
    }

    void OnEnable()
    {
        UpdateSelectionHelper();
    }

    void OnSelectionChange()
    {
        UpdateSelectionHelper();
    }

    private void UpdateSelectionHelper()
    {
        helpString = "";
        if (Selection.objects != null)
            helpString = "Number of objects selected: " + Selection.objects.Length;
    }

    void OnWizardCreate()
    {
        if (Selection.objects == null)
            return;

        int PostFix = StartNumber;
        foreach (var o in Selection.objects)
        {
            o.name = BaseName + PostFix;
            PostFix += Increment;
        }
    }

}
