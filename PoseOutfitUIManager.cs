﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PoseOutfitUIManager : MonoBehaviour {

    public Button[] outfitButtons;
    public Button bareButton;

    public void Select( int index )
    {
        var toSelect = bareButton;
        if( index > -1)
        {
            toSelect = outfitButtons[index];

            if( bareButton.gameObject.active )
                bareButton.interactable = true;
        }

        if (toSelect.gameObject.active)
            toSelect.interactable = false;

        for (int i = 0; i < outfitButtons.Length; i++)
        {
            if (outfitButtons[i] == toSelect) continue;

            if( outfitButtons[i].gameObject.active )
                outfitButtons[i].interactable = true;
        }
    }
}
