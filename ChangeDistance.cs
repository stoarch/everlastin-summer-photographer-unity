﻿using UnityEngine;
using System.Collections;

public class ChangeDistance : MonoBehaviour {

    public void SetDistance( float distance )
    {
        transform.localScale = new Vector3(distance, distance, distance);
    }
}
