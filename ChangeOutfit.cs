﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Outfit
{
    public string Caption;
    public SpriteRenderer Display;
}

public class ChangeOutfit : MonoBehaviour {

    public Outfit[] outfits;

    public Outfit ActiveOutfit
    {
        get
        {
            return outfits[activeOutfit];
        }
    }

    public int activeOutfit = 0;

    public void Deactivate()
    {
        if (activeOutfit < 0 || activeOutfit >= outfits.Length)
        {
            Debug.LogError(String.Format("Outfit index is invalid:{0} ({1}..{2})", activeOutfit, 0, outfits.Length));
            return;
        }

        outfits[activeOutfit].Display.enabled = false;
    }

    public void SetActive(int index)
    {
        if( index < 0 || index >= outfits.Length)
        {
            Debug.LogError(String.Format("Outfit index is invalid:{0} ({1}..{2})",index, 0, outfits.Length ));
            return;
        }
        
        outfits[activeOutfit].Display.enabled = false;
        activeOutfit = index;
        outfits[activeOutfit].Display.enabled = true;

    }
}
